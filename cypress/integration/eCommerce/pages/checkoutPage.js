class checkoutPage {
 setFirstName(){
    return cy.get('#billing_first_name')
 }
 setLastName() {
     return cy.get('#billing_last_name')
 }
 setCompany() {
     return cy.get('#billing_company')
 }
 setCountry(){
     return cy.get('#select2-billing_country-container')
 }
 setCity(){
     return cy.get('#billing_city')
 }
 setState(){
     return cy.get('#billing_state')
 }
 setPostCode(){
    return cy.get('#billing_postcode')
 }
setPhone(){
    return cy.get('#billing_phone')
}
setStreetCity(){
    return cy.get('#billing_address_1')
}
setEmail(){
    return cy.get('#billing_email')
}
setTermAndCondition(){
    return cy.get('.woocommerce-terms-and-conditions-wrapper > .form-row')
}
getTotal(){
    return cy.get('strong > .woocommerce-Price-amount > bdi')
}
getBillingProductName(){
    return cy.get('.product-name')
}
getBillingQty(){
    return cy.get('.product-quantity')
}
getBillingProductColor(){
    return cy.get('dd.variation-Color > p')
}
getBillingProductSize(){
   return cy.get('dd.variation-Size > p')
}
getPlaceOrderBtn(){
    return cy.get('#place_order')
}
getCouponlink(){
    return cy.get('.showcoupon')
}
getSearchBox(){
    return cy.get('.select2-search__field')
}


} export default checkoutPage