class loginPage{
    usernameInput() {
      return cy.get('#username')
    }
    passwordInput(){
      return cy.get('#password')
    }
    loginButton(){
      return cy.get(':nth-child(3) > .woocommerce-button')
    }
    logoutButton(){
        return cy.get('.woocommerce-MyAccount-navigation-link--customer-logout > a')
    }
    getdashboard(){
        return cy.get('.woocommerce-MyAccount-navigation-link--dashboard > a')
    }
    errorMessage(){
        return cy.get('.woocommerce-error')
    }
    newUsername(){
        return cy.get('#reg_username')
    }
    newEmailAddress(){
        return  cy.get('#reg_email')
    }
    newPassword(){
        return    cy.get('#reg_password')
    }
    registerButton(){
        return cy.get('.woocommerce-Button')
    }
    getProductlinnk(){
        return cy.get('.woocommerce-MyAccount-navigation-link--orders')
     }

}
export default loginPage