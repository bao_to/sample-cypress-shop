class productPage{

    getProductlink(){
       return cy.get('.woocommerce-MyAccount-navigation-link--orders > a')
    }
    getBrowseProductBtn() {
        return cy.get('.woocommerce-Button')
    }
    getSearchbar(){
      return cy.get('.noo-search')
    }
    getSearch() {
      return cy.get('.form-control')
    }
    getProductTitle(){
        return cy.get('.page-title')
    }
    // getColor(){
    //     return 
    // }
    getAdd2CartBtn(){
        return cy.get('.single_add_to_cart_button')
    }
    getProductprice(){
        return cy.get('.summary > .price > ins > .woocommerce-Price-amount > bdi')
    }
    getColor(){
        return cy.get('#color')
    }
    getSize() {
        return cy.get('#size')
    }
    getQuality(){
        cy.get('input').eq(2).should('have.value',1)
    }
    getChangeitem(){
        cy.get('input').eq(2).clear().type('1');
    }
    getCheckoutButton(){
        return cy.get('.single_add_to_cart_button')
    }
    getCartButton(){
        return cy.get('.cart-name-and-total')
    }
    getProductCartName(){
        return cy.get('.product-name > a')
    }
   getClearCart(){
    return cy.get('.empty-cart')
   }
   getUpdateCart(){
       return cy.get('[name="update_cart"]')
   }
   getUpdateMessage(){
    return cy.contains('Cart updated')
   }
   getProcessCheckout(){
    return cy.get('.wc-proceed-to-checkout > .checkout-button')
   }

}

export default productPage