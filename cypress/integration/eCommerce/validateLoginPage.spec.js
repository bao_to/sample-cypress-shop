
import loginPage from './pages/loginPage.js'

const login = new loginPage();

describe('Validate login page',function(){ 
   let testdata,user,usr,pwd;
   const login = new loginPage();

   beforeEach(function () {;
       cy.task('freshUser').then((object)=>{;
           user = object ;
       });
       cy.fixture('testdata.json').as ('testdata');
   
   });
it('Login with username and password',  function(){
    const login = new loginPage();
    cy.visit('/');
    cy.get('.woocommerce-store-notice__dismiss-link').click();
    cy.get('.pull-right > :nth-child(2) > a').click();
    login.usernameInput().should('be.visible').type(this.testdata.username);
    login.passwordInput().should('be.visible').type(this.testdata.password);
    login.loginButton().click();
    cy.contains(this.testdata.username).should('be.visible');
    login.logoutButton().should('be.visible').click();
   
   })
it('Login with blank username and password',function(){
   cy.visit('/');
   cy.get('.woocommerce-store-notice__dismiss-link').click();
   cy.get('.pull-right > :nth-child(2) > a').click();
   login.loginButton().click();
   login.errorMessage().should('be.visible');

})

it('Validate register new account',function(){
  let userId;
  cy.url().should('contains','/');
  login.newUsername().clear().type(user.username);
  login.newUsername().should(($input)=> { 
       usr = $input.val();
   });
  login.newEmailAddress().clear().type(user.email);
  login.newPassword().clear().type(user.password);
  login.newPassword().should(($input) => {
      pwd = $input.val();
  })
  login.registerButton().click();
  //validate register successful 

})
it('Validate get and set', function(){
   cy.get('#user_login').type(usr);
   cy.get('#user_pass').type(pwd);
   cy.get('#wp-submit').click();
})

})