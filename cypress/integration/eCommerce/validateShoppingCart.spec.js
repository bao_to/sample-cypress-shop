import loginPage from "./pages/loginPage"
import productPage from "./pages/productPage"
import checkoutPage from "./pages/checkoutPage"
const faker = require("faker");


describe('Validate Product and Shopping cart',function(){


    before(function(){
            cy.fixture('testdata.json').as ('data');
            });
      

    it('Validate select product',function(){
        const login = new loginPage();
        const product = new productPage();
        const file_name = "cypress/fixtures/productdata.json";

        cy.visit('/');
        cy.get('.woocommerce-store-notice__dismiss-link').click();
        cy.get('.pull-right > :nth-child(2) > a').click();
        
        login.usernameInput().type(this.data.username);
        login.passwordInput().type(this.data.password);
        login.loginButton().click();
        product.getProductlink().click();
        product.getBrowseProductBtn().click();
         
        product.getSearchbar().click();
        product.getSearch().type(this.data.productname + '{enter}');
    
        product.getProductTitle().should('have.text',this.data.productname)
      
        product.getAdd2CartBtn().should('be.visible');
        product.getColor().select(this.data.productcolor[1]).should('have.value','White');
        product.getSize().select(this.data.productsize[1]).should('have.value','M');
        cy.writeFile(file_name,{Productname:'Tokyo',Color:'White',Size:'M'},{flag:'a+'});

        product.getQuality();
        product.getCheckoutButton().click();
        product.getCartButton().click();
        product.getProductCartName().should('have.text',this.data.productname);
        product.getChangeitem();
        product.getUpdateCart().click();
        product.getUpdateMessage();
        product.getProcessCheckout().click();
   
    })

})

describe('Validate checkout page',function(){
    let user;
    const file_name = "cypress/fixtures/productdata.json";

    beforeEach(function(){
     
        cy.fixture('testdata.json').as ('data');
       
        });
  

    it.only('Validate checkout process',function(){
        const checkout = new checkoutPage();
        cy.login2Checkout(this.data.username,this.data.password);
        cy.url().should("eq",'https://shop.demoqa.com/checkout/');
   
        checkout.setFirstName().type(faker.name.firstName());
        checkout.setLastName().type(faker.name.lastName());
        checkout.setCompany().type(faker.company.companyName());
        checkout.setCountry().click();
        checkout.getSearchBox().type(faker.address.country()+"{enter}");
        checkout.setCity().type(faker.address.city());
        checkout.setStreetCity().type(faker.address.streetAddress());
        checkout.setPostCode().type(faker.address.zipCode());
        // checkout.setState().click();
        // checkout.getSearchBox().type(faker.address.zipCodeByState()+"{enter}");
        checkout.setPhone().type(faker.phone.phoneNumber());
        checkout.setEmail().type(faker.internet.exampleEmail());
        cy.readFile(file_name).then(function(item){
            
        })

    })

})
