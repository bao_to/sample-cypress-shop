// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// import loginPage from "cypress/integration/eCommerce/pages/loginPage.js";
const _ = require("lodash");  
Cypress.Commands.add('login2Checkout',(username,password)=>{
    cy.visit("/my-account")
    cy.get('#username').type(username)
    cy.get('#password').type(password)
    cy.get(':nth-child(3) > .woocommerce-button').click()
    cy.get('.cart-name-and-total').click()
    cy.get('.wc-proceed-to-checkout > .checkout-button').click()
    
})
Cypress.Commands.add("saveState", (key, value) => {
    cy.log(key, value);
    if(key.includes(">")){
      let keyItems = key.split(">");
      cy.readFile('cypress/fixtures/state/store.json').then((currState) => {
        let newState = currState;
        _.set(newState, keyItems, value);
        cy.writeFile('cypress/fixtures/state/store.json', newState);
      })
    }else{
    cy.readFile('cypress/fixtures/state/store.json').then((currState) => {
      currState[key] = value;
      cy.writeFile('cypress/fixtures/state/store.json', currState);
   })
  }
  
  });

  Cypress.Commands.add("parseXlsx", (inputFile) => {
    return cy.task('parseXlsx', { filePath: inputFile })
    });